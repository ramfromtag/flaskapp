FROM ubuntu:18.04

#Install dependencies
RUN apt-get update && \
 apt-get -y install python3-pip

COPY ./requirements.txt /app/requirements.txt

WORKDIR /app

RUN pip3 install -r requirements.txt

COPY . /app

ENTRYPOINT ["python3"]

CMD ["app.py"]